# Youtube Downloader

This is an experimental application to download mp3 music from youtube videos. For learning purposes only.

## Configure

You need to create a ``index.js`` file in src/config folder (if do not exists, create). So, include this piece of code on file:

````javascript
const config = {
    serverUrl : "http://SERVER_URL/",
}

export default config
````

## Run

To run this at development, just run ``react-native run-android``