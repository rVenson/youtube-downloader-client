import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#333',
    },
    infoContainer: {
      justifyContent: 'center',
      alignItems: 'center',
      width: '80%' 
    },
    title: {
      alignContent: 'center',
      backgroundColor: '#555'
    },
    titleText : {
      fontSize: 18,
      color: '#FFF',
      fontWeight: "bold",
      justifyContent: 'center',
    },
    defaultText: {
      color: '#DDD',
    },
    textInput : {
      backgroundColor: '#FFF',
      width: '80%',
      margin: 20
    },
    buttonContainer: {
      justifyContent: 'center',
      width: '60%',
    },
  })

  export default styles